﻿using System;
using System.Reflection;
using Synergize.Instructions;
using Synergize.Instructions.AccessFactories;
using Synergize.Instructions.Injectors;
using Synergize.Redirection;
using Synergize.Transpilers;

namespace Synergize.Sandbox
{
	internal static class Program
	{
		private delegate bool FullTestMethod(byte a, short b, int c);
		
		public static void Main()
		{
			MethodInfo original = AccessTools.Method(typeof(MethodContainer), nameof(MethodContainer.Original));
			MethodInfo originalInstance = AccessTools.Method<MethodContainer>(nameof(MethodContainer.OriginalInstance));
			MethodInfo replacement = AccessTools.Method(typeof(MethodContainer), nameof(MethodContainer.OriginalReplacement));
			MethodInfo replacementInstance = AccessTools.Method<MethodContainer>(nameof(MethodContainer.OriginalInstanceReplacement));
			MethodInfo postfix = AccessTools.Method(typeof(MethodContainer), nameof(MethodContainer.Postfix));
			MethodInfo attributePostfix = AccessTools.Method(typeof(MethodContainer), nameof(MethodContainer.AttributePostfix));

			using (Redirector.Redirect(original, replacement))
			{
				Console.WriteLine(MethodContainer.Original(3));
				Console.WriteLine(MethodContainer.OriginalReplacement(3));
			}

			MethodAccessFactory accessFactory = new MethodAccessFactory(original);

			Console.WriteLine("Original");
			Console.WriteLine(MethodContainer.Original(3));
			Console.WriteLine();

			LocalDeclarationInjector returnValueInitialization = new LocalDeclarationInjector(original.ReturnType, false);

			using (
				ITranspiler transpiler = new Transpiler(
					original,
					new PostfixInjectorHook(
						new PostReturnInjector(
							new ReferenceStackLocalLoadInjector(returnValueInitialization).Merge(Instruction.Call(postfix).ToInjector()),
							returnValueInitialization
						)
					)
				)
			)
			{
				using (transpiler.CreateHandle())
				{
					Console.WriteLine("Postfix injector");
					Console.WriteLine(MethodContainer.Original(3));
					Console.WriteLine();
				}
				
				Console.WriteLine("Original");
				Console.WriteLine(MethodContainer.Original(3));
				Console.WriteLine();

				using (transpiler.CreateHandle())
				{
					Console.WriteLine("Postfix injector 2");
					Console.WriteLine(MethodContainer.Original(3));
					Console.WriteLine();
				}
			}
			
			Console.WriteLine();

			using (
				ITranspiler transpiler = new Transpiler(
					original,
					new PostfixInjectorHook(
						new ValueReturnableInjector(
							new AttributeParameterWrapper(
								attributePostfix,
								attributePostfix.GetParameters()
							).Wrap(Instruction.Call(attributePostfix).ToInjector()),
							attributePostfix.ReturnType
						)
					)
				)
			)
			{
				using (transpiler.CreateHandle())
				{
					Console.WriteLine("Attribute injector");
					Console.WriteLine(MethodContainer.Original(3));
					Console.WriteLine();
				}
				
				Console.WriteLine("Original");
				Console.WriteLine(MethodContainer.Original(3));
				Console.WriteLine();
				
				using (transpiler.CreateHandle())
				{
					Console.WriteLine("Attribute injector");
					Console.WriteLine(MethodContainer.Original(3));
					Console.WriteLine();
				}
			}

			using (
				ITranspiler transpiler = new Transpiler(
					original,
					new PatternInjectorHook(	
						new EnumerableInjector(
							Instruction.Ldstr("Hi"),
							Instruction.Call(AccessTools.Method(typeof(Console), nameof(Console.WriteLine), typeof(string)))
						), 
						new[]
						{
							Instruction.Ldstr("A")
						},
						new IInstruction[0]
					)
				)
			)
			{
				using (transpiler.CreateHandle())
				{
					Console.WriteLine("Pattern injector");
					Console.WriteLine(MethodContainer.Original(3));
					Console.WriteLine();
				}
				
				Console.WriteLine("Original");
				Console.WriteLine(MethodContainer.Original(3));
				Console.WriteLine();
				
				using (transpiler.CreateHandle())
				{
					Console.WriteLine("Pattern injector");
					Console.WriteLine(MethodContainer.Original(3));
					Console.WriteLine();
				}
			}

			using (
				ITranspiler transpiler = new Transpiler(
					originalInstance,
					new PrefixInjectorHook(
						new AttributeParameterWrapper(
							originalInstance,
							replacementInstance.GetParameters()
						).Wrap(Instruction.Call(replacementInstance).ToInjector())
					)
				)
			)
			{
				MethodContainer obj = new MethodContainer();
				
				using (transpiler.CreateHandle())
				{
					Console.WriteLine("Instance attribute injector");
					Console.WriteLine(obj.OriginalInstance(3));
					Console.WriteLine();
				}
				
				Console.WriteLine("Original");
				Console.WriteLine(obj.OriginalInstance(3));
				Console.WriteLine();
				
				using (transpiler.CreateHandle())
				{
					Console.WriteLine("Instance attribute injector");
					Console.WriteLine(obj.OriginalInstance(3));
					Console.WriteLine();
				}
			}

			Func<byte, bool> methodInvocator = AccessTools.MethodInvocator<Func<byte, bool>>(
				typeof(MethodContainer), 
				"AccessToolsTestMethod"
			);
			Console.WriteLine(methodInvocator(1));

			Func<string> propertyInvocator = AccessTools.PropertyGetterInvocator<string>(
				typeof(MethodContainer), "AccessToolsTestProperty"
			);
			Console.WriteLine(propertyInvocator());

			Func<int> fieldInvocator = AccessTools.FieldGetterInvocator<int>(
				typeof(MethodContainer), 
				"accessToolsTestField"
			);
			Console.WriteLine(fieldInvocator());

			FullTestMethod lossyTestMethod = (FullTestMethod) AccessTools.LossyInvocator(
				typeof(FullTestMethod), 
				AccessTools.Method(typeof(MethodContainer), "StaticLossyTestMethod")
			);
			Console.WriteLine(lossyTestMethod(1, 2, 1));
			
			Func<object, FullTestMethod> lossyTestMethodInstantiator = 
				AccessTools.LossyInvocatorInstantiator<FullTestMethod>(
					AccessTools.Method(typeof(MethodContainer), "LossyTestMethod")
				);
			FullTestMethod instantiatedLossyTestMethod = lossyTestMethodInstantiator(new MethodContainer());
			Console.WriteLine(instantiatedLossyTestMethod(1, 2, 1));
		}
	}
}