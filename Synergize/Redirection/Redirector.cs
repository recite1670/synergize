﻿using System;
using System.Reflection;

namespace Synergize.Redirection
{
	public static class Redirector
	{
		public static IRedirectionPlatform Platform { get; }

		static Redirector()
		{
			IRedirectionEnvironment environment;

#if ANY
			PlatformID os = Environment.OSVersion.Platform;
			if (os == PlatformID.Win32S || os == PlatformID.Win32NT || os == PlatformID.Win32Windows || os == PlatformID.WinCE)
			{
#endif
#if (WINDOWS || ANY)
				environment = new WindowsRedirectionEnvironment();
#endif
#if ANY
			}
			else
			{
#endif
#if (!WINDOWS || ANY)
				environment = new DummyRedirectionEnvironment();
#endif
#if ANY
			}
#endif

			if (Type.GetType("Mono.Runtime") != null)
			{
				environment = new MonoRedirectionEnvironment(environment);
			}

#if ANY
			if (Environment.Is64BitProcess)
			{
#endif
#if (x64 || ANY)
				Platform = new Amd64RedirectionPlatform(environment);
#endif
#if ANY
			}
			else
			{
#endif
#if (x86 || ANY)
				Platform = new I386RedirectionPlatform(environment);
#endif
#if ANY
			}
#endif
		}

		public static IDisposable Redirect(MethodBase original, MethodBase replacement)
		{
			original.Prepare();
			replacement.Prepare();

			return Platform.Redirect(original, replacement);
		}
	}
}
