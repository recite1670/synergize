﻿using System;
using System.Reflection;

namespace Synergize.Redirection
{
	public class DummyRedirectionEnvironment : IRedirectionEnvironment
	{
		public void Prepare(MethodBase method)
		{
		}

		public IDisposable AllowWrite(IntPtr start, uint size)
		{
			return new DummyDisposable();
		}

		public IDisposable AllowExecute(IntPtr start, uint size)
		{
			return new DummyDisposable();
		}

		public void FlushInstructions(IntPtr start, uint size)
		{
		}

		private class DummyDisposable : IDisposable
		{
			public void Dispose()
			{
			}
		}
	}
}
