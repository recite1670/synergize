﻿using System;
using System.Reflection;

namespace Synergize.Redirection
{
	public interface IRedirectionEnvironment
	{
		void Prepare(MethodBase method);
		IDisposable AllowWrite(IntPtr start, uint size);
		IDisposable AllowExecute(IntPtr start, uint size);
		void FlushInstructions(IntPtr start, uint size);
	}
}
