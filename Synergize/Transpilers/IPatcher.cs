﻿using System.Collections.Generic;
using System.Reflection.Emit;

using Synergize.Instructions;

namespace Synergize.Transpilers
{
	public interface IPatcher
	{
		IEnumerable<IEmittableInstruction> Patch(IReadOnlyList<IEmittableInstruction> instructions, ILGenerator generator);
	}
}
