﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Synergize
{
	public static partial class AccessTools
	{
		public const BindingFlags FullAccessBindingFlags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

		private static TMember InheritedMemberSearch<TMember>(Type type, Func<Type, TMember> predicate) where TMember : MemberInfo
		{
			while (true)
			{
				TMember member = predicate(type);

				if (member != null)
				{
					return member;
				}

				if (type.BaseType == null)
				{
					return null;
				}

				type = type.BaseType;
			}
		}

		private static TMember[] InheritedMemberSearch<TMember>(Type type, Func<Type, TMember[]> predicate) where TMember : MemberInfo
		{
			List<TMember[]> allMembers = new List<TMember[]>();

			while (true)
			{
				TMember[] members = predicate(type);

				if (members.Length == 0 || type.BaseType == null)
				{
					return allMembers.ArrayFlatten<TMember, TMember[]>();
				}

				allMembers.Add(members);

				type = type.BaseType;
			}
		}

		#region Types

		public static Type[] Types(Type declarer)
		{
			return InheritedMemberSearch(declarer,
				typeLayer => typeLayer.GetNestedTypes(FullAccessBindingFlags));
		}

		public static Type[] Types<TDeclarer>()
		{
			return Types(typeof(TDeclarer));
		}

		public static Type Type(Type declarer, string name)
		{
			return InheritedMemberSearch(declarer,
				typeLayer => typeLayer.GetNestedType(name));
		}

		public static Type[] Type<TDeclarer>(string name)
		{
			return Types(typeof(TDeclarer));
		}

		#endregion

		#region Fields

		public static FieldInfo[] Fields<TDeclarer>() => Fields(typeof(TDeclarer));

		public static FieldInfo[] Fields(Type type) => InheritedMemberSearch(type, typeLayer => typeLayer.GetFields(FullAccessBindingFlags));

		public static FieldInfo Field<TDeclarer>(string name) => Field(typeof(TDeclarer), name);

		public static FieldInfo Field(Type type, string name) => InheritedMemberSearch(type, typeLayer => typeLayer.GetField(name, FullAccessBindingFlags));

		#endregion

		#region Properties

		public static PropertyInfo[] Properties<TDeclarer>() => Properties(typeof(TDeclarer));

		public static PropertyInfo[] Properties(Type type) => InheritedMemberSearch(type, typeLayer => typeLayer.GetProperties(FullAccessBindingFlags));

		public static PropertyInfo Property<TDeclarer>(string name) => Property(typeof(TDeclarer), name);

		public static PropertyInfo Property(Type type, string name) => InheritedMemberSearch(type, typeLayer => typeLayer.GetProperty(name, FullAccessBindingFlags));

		#endregion

		#region Methods

		public static MethodInfo[] Methods<TDeclarer>() => Methods(typeof(TDeclarer));

		public static MethodInfo[] Methods(Type type) => InheritedMemberSearch(type, typeLayer => typeLayer.GetMethods(FullAccessBindingFlags));

		public static MethodInfo Method<TDeclarer>(string name) => Method(typeof(TDeclarer), name);

		public static MethodInfo Method<TDeclarer>(string name, params Type[] parameters) => Method(typeof(TDeclarer), name, parameters);

		public static MethodInfo Method<TDeclarer, TDelegate>(string name, out MethodInfo invoke, out Type[] parameters) where TDelegate : Delegate =>
			Method<TDelegate>(typeof(TDeclarer), name, out invoke, out parameters);

		public static MethodInfo Method(Type type, string name) => InheritedMemberSearch(type, typeLayer => typeLayer.GetMethod(name, FullAccessBindingFlags));

		public static MethodInfo Method(Type type, string name, params Type[] parameters) =>
			InheritedMemberSearch(type, typeLayer => typeLayer.GetMethod(name, FullAccessBindingFlags, null, parameters, null));

		private static void ExtractDelegateInvoke(Type delegateType, out MethodInfo invoke, out Type[] parameters)
		{
			if (!typeof(Delegate).IsAssignableFrom(delegateType))
			{
				throw new ArgumentException("Delegate type must be a subclass of " + nameof(delegateType), nameof(delegateType));
			}

			invoke = Method(delegateType, "Invoke");
			ParameterInfo[] parameterInfos = invoke.GetParameters();
			parameters = new Type[parameterInfos.Length];

			for (int i = 0; i < parameterInfos.Length; ++i)
			{
				parameters[i] = parameterInfos[i].ParameterType;
			}
		}

		public static MethodInfo Method(Type delegateType, Type type, string name, out MethodInfo invoke, out Type[] parameters)
		{
			ExtractDelegateInvoke(delegateType, out invoke, out parameters);

			return Method(type, name, parameters);
		}

		public static MethodInfo Method<TDelegate>(Type type, string name, out MethodInfo invoke, out Type[] parameters) where TDelegate : Delegate
		{
			return Method(typeof(TDelegate), type, name, out invoke, out parameters);
		}

		#endregion

		#region Constructors

		public static ConstructorInfo[] Constructors<TDeclarer>() => Constructors(typeof(TDeclarer));

		public static ConstructorInfo[] Constructors(Type type) => InheritedMemberSearch(type, typeLayer => typeLayer.GetConstructors(FullAccessBindingFlags));

		public static ConstructorInfo Constructor<TDeclarer>() => Constructor(typeof(TDeclarer));

		public static ConstructorInfo Constructor<TDeclarer>(Type[] parameters) => Constructor(typeof(TDeclarer), parameters);

		public static ConstructorInfo Constructor(Type type) => InheritedMemberSearch(type, typeLayer => typeLayer.GetConstructors(FullAccessBindingFlags).Single());

		public static ConstructorInfo Constructor(Type type, params Type[] parameters) =>
			InheritedMemberSearch(type, typeLayer => typeLayer.GetConstructor(FullAccessBindingFlags, null, parameters, null));

		public static ConstructorInfo Constructor(Type delegateType, Type type)
		{
			ExtractDelegateInvoke(delegateType, out _, out Type[] parameters);

			return Constructor(type, parameters);
		}

		public static ConstructorInfo Constructor<TDelegate>(Type type, out MethodInfo invoke, out Type[] parameters)
		{
			ExtractDelegateInvoke(typeof(TDelegate), out invoke, out parameters);

			return Constructor(type, parameters);
		}

		#endregion
	}
}
