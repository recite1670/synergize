﻿namespace Synergize
{
	/// <summary>
	///     For use when using <code>nameof</code> on a type or method requiring generic arguments.
	/// </summary>
	internal enum GenericPlaceholder
	{
	}
}
