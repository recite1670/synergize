﻿using System.Linq;
using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public abstract class OperandInstruction<TOperand> : OpCodeInstruction
	{
		protected abstract int OperandSize { get; }

		public TOperand Operand { get; }

		public override int Size => base.Size + OperandSize;

		protected OperandInstruction(OpCode operation, TOperand operand) : base(operation)
		{
			Operand = operand;
		}

		protected abstract void OperandEmit(ILGenerator generator);
		protected abstract bool OperandEquals(TOperand other);

		public override void Emit(ILGenerator generator)
		{
			foreach (IEmittable handler in ExceptionHandlers)
			{
				handler.Emit(generator);
			}

			OperandEmit(generator);
		}

		public override bool Equals(IInstruction other)
		{
			OperandInstruction<TOperand> otherOperandInstruction = other as OperandInstruction<TOperand>;
			return otherOperandInstruction != null && base.Equals(other) && OperandEquals(otherOperandInstruction.Operand);
		}

		public override string ToString()
		{
			return $"IL_{Offset:x4} {(ExceptionHandlers.Count == 0 ? string.Empty : string.Join(" ", ExceptionHandlers.Select(x => "." + x)) + " ")}{Operation} {Operand}";
		}
	}
}
