﻿using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class DoubleOperandInstruction : EquatableOperandInstruction<double>
	{
		protected override int OperandSize { get; } = sizeof(double);

		public DoubleOperandInstruction(OpCode operation, double operand) : base(operation, operand)
		{
		}

		protected override void OperandEmit(ILGenerator generator)
		{
			generator.Emit(Operation, Operand);
		}
	}
}
