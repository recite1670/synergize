﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public class LocalStoreInjector : IStackLocalInjector
	{
		public IInjector Value { get; }
		public IStackLocal StackLocal { get; }

		public LocalStoreInjector(IStackLocal stackLocal, IInjector value)
		{
			StackLocal = stackLocal;
			Value = value;
		}

		public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
		{
			if (StackLocal.Local == null)
			{
				throw new InvalidOperationException("The stack local must be injected before attempting to load it.");
			}

			foreach (IEmittableInstruction il in Value.Inject(generator))
			{
				yield return il;
			}

			yield return Instruction.Stloc(StackLocal.Local);
		}
	}
}
