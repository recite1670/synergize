﻿using System.Collections.Generic;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public class EnumerableInjector : IInjector
	{
		private readonly IEnumerable<IEmittableInstruction> _items;

		public EnumerableInjector(IEnumerable<IEmittableInstruction> items)
		{
			_items = items;
		}

		public EnumerableInjector(params IEmittableInstruction[] items) : this((IEnumerable<IEmittableInstruction>) items)
		{
		}

		public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator) => _items.Duplicate();
	}
}
