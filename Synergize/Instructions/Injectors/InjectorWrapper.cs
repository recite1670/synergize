﻿using System.Collections.Generic;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public class InjectorWrapper : IWrapper
	{
		private readonly IInjector _post;
		private readonly IInjector _pre;

		public InjectorWrapper(IInjector pre, IInjector post)
		{
			_pre = pre;
			_post = post;
		}

		public IInjector Wrap(IInjector wrapped)
		{
			return new WrapperInjector(wrapped, _pre, _post);
		}

		private class WrapperInjector : IInjector
		{
			private readonly IInjector _post;
			private readonly IInjector _pre;
			private readonly IInjector _wrapped;

			public WrapperInjector(IInjector wrapped, IInjector pre, IInjector post)
			{
				_wrapped = wrapped;
				_pre = pre;
				_post = post;
			}

			public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
			{
				if (_pre != null)
				{
					foreach (IEmittableInstruction il in _pre.Inject(generator))
					{
						yield return il;
					}
				}

				foreach (IEmittableInstruction il in _wrapped.Inject(generator))
				{
					yield return il;
				}

				if (_post != null)
				{
					foreach (IEmittableInstruction il in _post.Inject(generator))
					{
						yield return il;
					}
				}
			}
		}
	}
}
