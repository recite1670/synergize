﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;

using Synergize.Instructions.AccessFactories;

namespace Synergize.Instructions.Injectors
{
	public class AttributeParameterWrapper : IWrapper
	{
		public IReadOnlyList<IWrapper> Parameters { get; }

		public IMethodAccessFactory SourceAccessFactory { get; }
		public MethodBase SourceSite { get; }

		public AttributeParameterWrapper(MethodBase sourceSite, IReadOnlyList<ParameterInfo> targetParameters)
		{
			SourceSite = sourceSite;

			SourceAccessFactory =
				sourceSite.IsStatic
					? new MethodAccessFactory(sourceSite)
					: new InstanceMethodAccessFactory(sourceSite);

			Parameters = new ReadOnlyCollection<IWrapper>(targetParameters.ArraySelect((parameter, i) =>
				parameter.GetCustomAttribute<ImportAttribute>().CreateWrapper(SourceAccessFactory, parameter)));
		}

		public IInjector Wrap(IInjector wrapped)
		{
			// Despite being a reverse for, the parameter load order is retained.
			// The post-execution is last-to-first parameter.
			for (int i = Parameters.Count - 1; i >= 0; --i)
			{
				wrapped = Parameters[i].Wrap(wrapped);
			}

			return wrapped;
		}
	}
}
