﻿using System;
using System.Reflection;

using Synergize.Instructions.AccessFactories;

namespace Synergize.Instructions.Injectors
{
	public class ImportLocalAttribute : ImportAttribute
	{
		public ushort Index { get; }

		public ImportLocalAttribute(ushort index)
		{
			Index = index;
		}

		public override IWrapper CreateWrapper(IMethodAccessFactory accessFactory, ParameterInfo self)
		{
			if (Index >= accessFactory.Locals.Count)
			{
				throw new IndexOutOfRangeException("Index exceeds the number of local variables.");
			}

			if (!self.ParameterType.StripRef(out bool byRef).IsAssignableFrom(accessFactory.Locals[Index].LocalType))
			{
				throw new ArgumentException("Parameter type is not assignable from the local type.", nameof(self));
			}

			return new InjectorWrapper(byRef
					? accessFactory.LoadLocalToReference(Index)
					: accessFactory.LoadLocal(Index),
				null);
		}
	}
}
