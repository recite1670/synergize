﻿using System.Collections.Generic;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public class InstructionInjector : IInjector
	{
		public IEmittableInstruction Content { get; }

		public InstructionInjector(IEmittableInstruction content)
		{
			Content = content;
		}

		public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
		{
			yield return Content;
		}
	}
}
