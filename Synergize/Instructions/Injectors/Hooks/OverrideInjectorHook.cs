﻿using System.Collections.Generic;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public class OverrideInjectorHook : IInjectorHook
	{
		public IInjector Injector { get; }

		public OverrideInjectorHook(IInjector injector)
		{
			Injector = injector;
		}

		public IEnumerable<IEmittableInstruction> Patch(IReadOnlyList<IEmittableInstruction> instructions, ILGenerator generator)
		{
			foreach (IEmittableInstruction il in Injector.Inject(generator))
			{
				yield return il;
			}

			yield return Instruction.Ret;
		}
	}
}
