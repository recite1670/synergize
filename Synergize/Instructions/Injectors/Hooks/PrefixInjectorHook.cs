﻿using System.Collections.Generic;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public class PrefixInjectorHook : IInjectorHook
	{
		public IInjector Injector { get; }

		public PrefixInjectorHook(IInjector injector)
		{
			Injector = injector;
		}

		public IEnumerable<IEmittableInstruction> Patch(IReadOnlyList<IEmittableInstruction> instructions, ILGenerator generator)
		{
			foreach (IEmittableInstruction il in Injector.Inject(generator))
			{
				yield return il;
			}

			foreach (IEmittableInstruction il in instructions)
			{
				yield return il;
			}
		}
	}
}
