﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;

using Synergize.Enumerables;

namespace Synergize.Instructions.Injectors
{
	public class ForLoopInjector : IInjector
	{
		public IInjector Body { get; }
		public IInjector Condition { get; }
		public IInjector Initial { get; }
		public IInjector PostIterate { get; }

		public ForLoopInjector(IInjector initial, IInjector condition, IInjector body, IInjector postIterate)
		{
			Initial = initial;
			Condition = condition;
			Body = body;
			PostIterate = postIterate;
		}

		public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
		{
			// Initial must be BEFORE condition, then BEFORE body, then BEFORE post iterate because a stack local might be declared.

			foreach (IEmittableInstruction il in Initial.Inject(generator))
			{
				yield return il;
			}

			List<IEmittableInstruction> condition = Condition.Inject(generator).ToList();
			if (condition.Count == 0)
			{
				throw new InvalidOperationException("Condition is empty.");
			}

			yield return Instruction.Br(condition[0]);

			FirstExtractorEnumerable<IEmittableInstruction> bodyFirst = new FirstExtractorEnumerable<IEmittableInstruction>(Body.Inject(generator));
			foreach (IEmittableInstruction il in bodyFirst)
			{
				yield return il;
			}

			FirstExtractorEnumerable<IEmittableInstruction> postIterateFirst = new FirstExtractorEnumerable<IEmittableInstruction>(PostIterate.Inject(generator));
			foreach (IEmittableInstruction il in postIterateFirst)
			{
				yield return il;
			}

			foreach (IEmittableInstruction il in condition)
			{
				yield return il;
			}

			yield return Instruction.Brtrue(bodyFirst.Extracted
				? bodyFirst.ExtractedValue
				: postIterateFirst.Extracted
					? postIterateFirst.ExtractedValue
					: condition[0]);
		}
	}
}
