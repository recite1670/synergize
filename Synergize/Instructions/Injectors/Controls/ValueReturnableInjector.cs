﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace Synergize.Instructions.Injectors
{
	public class ValueReturnableInjector : IInjector
	{
		public IInjector HandledReturn { get; }
		public Type HandledReturnType { get; }
		public Type ReturnType { get; }

		public ValueReturnableInjector(IInjector handledReturn, Type handledReturnType)
		{
			Type genericDefinition = typeof(HandledReturn<>);
			if (!handledReturnType.IsGenericType || handledReturnType.GetGenericTypeDefinition() != genericDefinition)
			{
				throw new ArgumentException($"Site does not return a {genericDefinition.MakeGenericType(handledReturnType).Name}.", nameof(handledReturnType));
			}

			ReturnType = handledReturnType.GenericTypeArguments[0];
			HandledReturnType = handledReturnType;
			HandledReturn = handledReturn;
		}

		public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
		{
			LocalBuilder originalReturnLocal = generator.DeclareLocal(ReturnType);
			LocalBuilder handledReturnLocal = generator.DeclareLocal(HandledReturnType);

			yield return Instruction.Stloc(originalReturnLocal);

			foreach (IEmittableInstruction il in HandledReturn.Inject(generator))
			{
				yield return il;
			}

			yield return Instruction.Stloc(handledReturnLocal);

			IEmittableInstruction loadOriginal = Instruction.Ldloc(originalReturnLocal);

			yield return Instruction.Ldloca(handledReturnLocal);
			yield return Instruction.Ldfld(AccessTools.Field(HandledReturnType, nameof(HandledReturn<GenericPlaceholder>.IsHandled)));
			yield return Instruction.Brfalse(loadOriginal);

			yield return Instruction.Ldloca(handledReturnLocal);
			yield return Instruction.Ldfld(AccessTools.Field(HandledReturnType, nameof(HandledReturn<GenericPlaceholder>.ReturnValue)));
			yield return Instruction.Ret;

			yield return loadOriginal;
		}
	}
}
