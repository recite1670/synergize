﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;

using Synergize.Enumerables;

namespace Synergize.Instructions.Injectors
{
	public class WhileLoopInjector : IInjector
	{
		public IInjector Body { get; }
		public IInjector Condition { get; }

		public WhileLoopInjector(IInjector condition, IInjector body)
		{
			Condition = condition;
			Body = body;
		}

		public IEnumerable<IEmittableInstruction> Inject(ILGenerator generator)
		{
			// Condition must be evaluated BEFORE body because a stack local might be declared.

			List<IEmittableInstruction> condition = Condition.Inject(generator).ToList();
			if (condition.Count == 0)
			{
				throw new InvalidOperationException("Condition is empty.");
			}

			yield return Instruction.Br(condition[0]);

			FirstExtractorEnumerable<IEmittableInstruction> bodyFirst = new FirstExtractorEnumerable<IEmittableInstruction>(Body.Inject(generator));
			foreach (IEmittableInstruction il in bodyFirst)
			{
				yield return il;
			}

			foreach (IEmittableInstruction il in condition)
			{
				yield return il;
			}

			yield return Instruction.Brtrue(bodyFirst.Extracted
				? bodyFirst.ExtractedValue
				: condition[0]);
		}
	}
}
