﻿using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class IntOperandInstruction : EquatableOperandInstruction<int>
	{
		protected override int OperandSize { get; } = sizeof(int);

		public IntOperandInstruction(OpCode operation, int operand) : base(operation, operand)
		{
		}

		protected override void OperandEmit(ILGenerator generator)
		{
			generator.Emit(Operation, Operand);
		}
	}
}
