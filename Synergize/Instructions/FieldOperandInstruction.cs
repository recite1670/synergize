﻿using System.Reflection;
using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class FieldOperandInstruction : OperandInstruction<FieldInfo>
	{
		protected override int OperandSize { get; } = 4;

		public FieldOperandInstruction(OpCode operation, FieldInfo operand) : base(operation, operand)
		{
		}

		protected override void OperandEmit(ILGenerator generator)
		{
			generator.Emit(Operation, Operand);
		}

		protected override bool OperandEquals(FieldInfo other)
		{
			return Operand == other;
		}
	}
}
