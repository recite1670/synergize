﻿using System;
using System.Reflection.Emit;

namespace Synergize.Instructions.ExceptionBlocks
{
	public class CatchClauseBlock : ExceptionClauseBlock
	{
		public Type CatchingType { get; }

		public CatchClauseBlock(ExceptionBlock startTryBlock, EndHandlerBlock endTryBlock) : base(startTryBlock, endTryBlock)
		{
		}

		public CatchClauseBlock(ExceptionBlock startTryBlock, EndHandlerBlock endTryBlock, Type catchingType) : base(startTryBlock, endTryBlock)
		{
			CatchingType = catchingType;
		}

		public override void Emit(ILGenerator generator)
		{
			if (CatchingType == null)
			{
				generator.BeginFaultBlock();
			}
			else
			{
				generator.BeginCatchBlock(CatchingType);
			}
		}

		public override string ToString() => "catch";
	}
}
