﻿using System.Reflection.Emit;

namespace Synergize.Instructions.ExceptionBlocks
{
	public abstract class ExceptionClauseBlock : IEmittable
	{
		public EndHandlerBlock EndTryBlock { get; }
		public ExceptionBlock StartTryBlock { get; }

		protected ExceptionClauseBlock(ExceptionBlock startTryBlock, EndHandlerBlock endTryBlock)
		{
			StartTryBlock = startTryBlock;
			EndTryBlock = endTryBlock;
		}

		public abstract void Emit(ILGenerator generator);

		public abstract override string ToString();
	}
}
