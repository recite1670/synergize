﻿using System.Reflection.Emit;

namespace Synergize.Instructions.ExceptionBlocks
{
	public class FilterClauseBlock : ExceptionClauseBlock
	{
		public FilterClauseBlock(ExceptionBlock startTryBlock, EndHandlerBlock endTryBlock) : base(startTryBlock, endTryBlock)
		{
		}

		public override void Emit(ILGenerator generator)
		{
			generator.BeginExceptFilterBlock();
		}

		public override string ToString() => "filter catch";
	}
}
