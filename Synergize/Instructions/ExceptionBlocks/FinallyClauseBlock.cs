﻿using System.Reflection.Emit;

namespace Synergize.Instructions.ExceptionBlocks
{
	public class FinallyClauseBlock : ExceptionClauseBlock
	{
		public FinallyClauseBlock(ExceptionBlock startTryBlock, EndHandlerBlock endTryBlock) : base(startTryBlock, endTryBlock)
		{
		}

		public override void Emit(ILGenerator generator)
		{
			generator.BeginFinallyBlock();
		}

		public override string ToString() => "finally";
	}
}
