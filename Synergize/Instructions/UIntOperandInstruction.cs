﻿using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public class UIntOperandInstruction : EquatableOperandInstruction<uint>
	{
		protected override int OperandSize { get; } = sizeof(uint);

		public UIntOperandInstruction(OpCode operation, uint operand) : base(operation, operand)
		{
		}

		protected override void OperandEmit(ILGenerator generator)
		{
			generator.Emit(Operation, Operand);
		}
	}
}
