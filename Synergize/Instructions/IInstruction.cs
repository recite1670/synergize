﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace Synergize.Instructions
{
	public interface IInstruction : IEquatable<IInstruction>
	{
		List<IEmittable> ExceptionHandlers { get; }

		int Offset { get; set; }

		OpCode Operation { get; }
		int Size { get; }
	}
}
