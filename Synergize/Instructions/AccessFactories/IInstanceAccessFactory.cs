﻿using Synergize.Instructions.Injectors;

namespace Synergize.Instructions.AccessFactories
{
	public interface IInstanceAccessFactory : IAccessFactory
	{
		IInjector LoadInstance { get; }
	}
}
