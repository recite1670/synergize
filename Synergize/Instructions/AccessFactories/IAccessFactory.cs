﻿using System;
using System.Reflection;

using Synergize.Instructions.Injectors;

namespace Synergize.Instructions.AccessFactories
{
	public interface IAccessFactory
	{
		Type TypeSite { get; }

		IInjector LoadField(FieldInfo field);

		IInjector LoadField(string name);

		IInjector LoadFieldToReference(FieldInfo field);

		IInjector LoadFieldToReference(string name);

		IInjector StoreField(FieldInfo field, IInjector value);

		IInjector StoreField(string name, IInjector value);

		IInjector StoreFieldFromReference(FieldInfo field, IInjector value);

		IInjector StoreFieldFromReference(string name, IInjector value);

		IInjector LoadProperty(PropertyInfo property);

		IInjector LoadProperty(string name);

		IInjector StoreProperty(PropertyInfo property, IInjector value);

		IInjector StoreProperty(string name, IInjector value);

		IInjector StorePropertyFromReference(PropertyInfo property, IInjector value);

		IInjector StorePropertyFromReference(string name, IInjector value);
	}
}
