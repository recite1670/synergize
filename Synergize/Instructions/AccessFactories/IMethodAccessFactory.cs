﻿using System.Collections.Generic;
using System.Reflection;

using Synergize.Instructions.Injectors;

namespace Synergize.Instructions.AccessFactories
{
	public interface IMethodAccessFactory : IAccessFactory
	{
		IReadOnlyList<LocalVariableInfo> Locals { get; }
		MethodBase MethodSite { get; }

		IReadOnlyList<ParameterInfo> Parameters { get; }

		IInjector LoadArgument(ushort index);

		IInjector LoadArgument(string name);

		IInjector LoadArgumentToReference(ushort index);

		IInjector LoadArgumentToReference(string name);

		IInjector LoadArgumentFromReference(ushort index);

		IInjector LoadArgumentFromReference(string name);

		IInjector StoreArgument(ushort index, IInjector value);

		IInjector StoreArgument(string name, IInjector value);

		IInjector StoreArgumentFromReference(ushort index, IInjector value);

		IInjector StoreArgumentFromReference(string name, IInjector value);

		IInjector LoadLocal(ushort index);

		IInjector LoadLocal(LocalVariableInfo local);

		IInjector LoadLocalToReference(ushort index);

		IInjector LoadLocalToReference(LocalVariableInfo local);

		IInjector LoadLocalFromReference(ushort index);

		IInjector LoadLocalFromReference(LocalVariableInfo local);

		IInjector StoreLocal(ushort index, IInjector value);

		IInjector StoreLocal(LocalVariableInfo local, IInjector value);

		IInjector StoreLocalFromReference(ushort index, IInjector value);

		IInjector StoreLocalFromReference(LocalVariableInfo local, IInjector value);
	}
}
