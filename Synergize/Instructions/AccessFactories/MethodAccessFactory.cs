﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;

using Synergize.Instructions.Injectors;

namespace Synergize.Instructions.AccessFactories
{
	public class MethodAccessFactory : AccessFactory, IMethodAccessFactory
	{
		public MethodBase MethodSite { get; }
		public IReadOnlyList<ParameterInfo> Parameters { get; }
		public IReadOnlyList<LocalVariableInfo> Locals { get; }

		public MethodAccessFactory(MethodBase methodSite) : base(methodSite.DeclaringType)
		{
			MethodBody body = methodSite.GetMethodBody();
			if (body == null)
			{
				throw new ArgumentException("Method returned a null body.", nameof(methodSite));
			}

			MethodSite = methodSite;
			Parameters = new ReadOnlyCollection<ParameterInfo>(methodSite.GetParameters());
			Locals = new ReadOnlyCollection<LocalVariableInfo>(body.LocalVariables);
		}

		public IInjector LoadArgument(ushort index)
		{
			GetArgument(ref index);

			return Instruction.Ldarg(index).ToInjector();
		}

		public IInjector LoadArgument(string name) => LoadArgument(GetArgument(name));

		public IInjector LoadArgumentToReference(ushort index)
		{
			GetArgument(ref index);

			return Instruction.Ldarga(index).ToInjector();
		}

		public IInjector LoadArgumentToReference(string name) => LoadArgumentToReference(GetArgument(name));

		public IInjector LoadArgumentFromReference(ushort index)
		{
			GetArgument(ref index);

			return new EnumerableInjector(Instruction.Ldarg(index), Instruction.Ldind(Parameters[index].ParameterType.StripRef(out _)));
		}

		public IInjector LoadArgumentFromReference(string name) => LoadArgumentFromReference(GetArgument(name));

		public IInjector StoreArgument(ushort index, IInjector value)
		{
			GetArgument(ref index);

			return value.Merge(Instruction.Starg(index).ToInjector());
		}

		public IInjector StoreArgument(string name, IInjector value) => StoreArgument(GetArgument(name), value);

		public IInjector StoreArgumentFromReference(ushort index, IInjector value)
		{
			GetArgument(ref index);

			return StoreArgument(index, value.Merge(Instruction.Ldind(Parameters[index].ParameterType.StripRef(out _)).ToInjector()));
		}

		public IInjector StoreArgumentFromReference(string name, IInjector value) => StoreArgument(GetArgument(name), value);

		public IInjector LoadLocal(ushort index) => Instruction.Ldloc(index).ToInjector();

		public IInjector LoadLocal(LocalVariableInfo local) => LoadLocal((ushort) local.LocalIndex);

		public IInjector LoadLocalToReference(ushort index) => Instruction.Ldloca(index).ToInjector();

		public IInjector LoadLocalToReference(LocalVariableInfo local) => LoadLocalToReference((ushort) local.LocalIndex);

		public IInjector LoadLocalFromReference(ushort index) => new EnumerableInjector(Instruction.Ldloc(index), Instruction.Ldind(Locals[index].LocalType.StripRef(out _)));

		public IInjector LoadLocalFromReference(LocalVariableInfo local) => LoadLocalFromReference((ushort) local.LocalIndex);

		public IInjector StoreLocal(ushort index, IInjector value) => value.Merge(Instruction.Stloc(index).ToInjector());

		public IInjector StoreLocal(LocalVariableInfo local, IInjector value) => StoreLocal((ushort) local.LocalIndex, value);

		public IInjector StoreLocalFromReference(ushort index, IInjector value)
		{
			GetArgument(ref index);

			return StoreLocal(index, value.Merge(Instruction.Ldind(Parameters[index].ParameterType.StripRef(out _)).ToInjector()));
		}

		public IInjector StoreLocalFromReference(LocalVariableInfo local, IInjector value) => StoreLocalFromReference((ushort) local.LocalIndex, value);

		private void GetArgument(ref ushort index)
		{
			int argumentCount = Parameters.Count;
			if (index >= argumentCount)
			{
				throw new InvalidOperationException($"The method only has {argumentCount} arguments.");
			}

			if (!MethodSite.IsStatic)
			{
				++index;
			}
		}

		private ushort GetArgument(string name)
		{
			int index = Parameters.IndexOf(x => x.Name == name);
			if (index == -1)
			{
				throw new InvalidOperationException("The method has no argument with that name.");
			}

			return (ushort) index;
		}
	}
}
