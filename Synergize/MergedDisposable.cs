﻿using System;

namespace Synergize
{
	public class MergedDisposable : IDisposable
	{
		private readonly IDisposable _concatenated;
		private readonly IDisposable _original;

		public MergedDisposable(IDisposable original, IDisposable concatenated)
		{
			_original = original;
			_concatenated = concatenated;
		}

		public void Dispose()
		{
			_original.Dispose();
			_concatenated.Dispose();
		}
	}
}
